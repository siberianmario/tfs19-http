package tfs.scala.http.client

import java.util.UUID

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import akka.testkit.TestKit
import org.json4s.jackson.Serialization.write
import org.scalatest.Matchers
import tfs.scala.http.client.util.HttpClientTest
import tfs19.scala.http.client.Json4sSupport

case class User(id: UUID, firstName: String, lastName: String, languages: Seq[String])
case class ServerError(code: String, message: String)

// http://tfs19.shapeless.space:8080/docs
class HttpClientJsonTest extends TestKit(ActorSystem()) with HttpClientTest with Json4sSupport with Matchers {
  implicit val materializer = ActorMaterializer()

  behavior of "HttpClientJson"

  private val id = UUID.randomUUID()
  val user = User(
    id = id,
    firstName = "John",
    lastName = "Doe",
    languages = Seq("Scala")
  )

  it should "create new user and get it" in {
    Http()
      .singleRequest(HttpRequest(
        uri = Uri("http://tfs19.shapeless.space:8080/users"),
        method = HttpMethods.POST,
        entity = HttpEntity(ContentTypes.`application/json`, write(user))
      ))
      .flatMap(Unmarshal(_).to[User])
      .futureValue shouldBe user
  }

  it should "get user by id" in {
    Http()
      .singleRequest(HttpRequest(HttpMethods.GET, Uri(s"http://tfs19.shapeless.space:8080/users/${id.toString}")))
      .flatMap(Unmarshal(_).to[User])
      .futureValue shouldBe user
  }

  it should "get users list" in {

  }

  it should "update user firstName, lastName and languages" in {

  }
}